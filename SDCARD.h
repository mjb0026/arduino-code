
#ifndef SDCARD_H
#define SDCARD_H
#include "Arduino.h"
#include <SD.h>
#include <SPI.h>

#define ChipSelect = 4


class Sdcard {
  public:
  Sdcard();
  void WriteFile(long outData);
  void ReadFile();
  private:


};



#endif
