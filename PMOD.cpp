#include "Arduino.h"
#include "PMOD.h"
#include <Wire.h>

/*
  @param value - double precision value to be printed

  Prints floating point values using scientific notation (no endline character) 
    to serial output
*/
void printScientific(double value) {
  double temp = abs(value);
  int i = 0;
  
  while (temp < 1 && temp != 0) {
    temp *= 10;
    i++;
  }
  Serial.print(value * pow(10, i), 6); Serial.print(F("e-")); Serial.print(i);
}

// Pmod class definitions

Pmod::Pmod() {
  Wire.begin();
}

Pmod::~Pmod() {
  Serial.end();
}

/*
  @param Addr - Address to write data too
  @param Memory - contains the data to be changed
*/
bool Pmod::SetByte(byte Addr, byte Memory) {
  int i;

  // Attempt to set byte up to I2C_ATTEMPTS times
  for (i = 0; i < I2C_ATTEMPTS; i++) {
    Wire.beginTransmission(Bus);                  //Begins I2C by calling PMOD IA Serial bus
    Wire.write(Addr);                             //sends the address for where the memory needs to be stored
    Wire.write(Memory);                             //sends memory
    byte Status = Wire.endTransmission();         //ends transmission and recieves value indicating status, 0 = Success
    if (Status == 0) {
      return true;
    }

    Serial.print(F("Warning: I2C failed attempt in SetByte("));
    Serial.print(Memory, HEX);
    Serial.print(F(", "));
    Serial.print(Addr, HEX);
    Serial.println(F(")"));
    delay(20);
  }

  Serial.print(F("    ")); Serial.print(i); Serial.println(F(" failed attempts"));
  Serial.print(F("Error: SetByte failed setting ")); 
  Serial.print(Memory, HEX); 
  Serial.print(F(" to address "));
  Serial.println(Addr, HEX);
  return false;
}

/*
  @param Addr - Address to write data too
  @param *ptr - ptr where the byte will be stored after succesful transmission
*/
bool Pmod::GetByte(byte Addr, byte *ptr) {
  int i;
  
  Wire.beginTransmission(Bus);                //Begins I2C by calling PMOD IA Serial bus
  Wire.write(Addr_ptr);                       //Send the address pointer addres for where needs to be stored
  Wire.write(Addr);                           //sends the address for what byte needs to me received
  byte Status = Wire.endTransmission();       //ends transmission and recieves value indicating status, 0 = Success
  Wire.requestFrom(Bus, 1);                   //requests to receive 1 byte from PMOD IA
  
  for (i = 0; i < I2C_ATTEMPTS; i++) {
    delay(10);                                //prevent reading the channel before the PMOD sends back data
    if (Wire.available())                     //returns numbers of bytes available for retrieval with read()
    {
      *ptr = Wire.read();                     //points to where the readable data is
      return true;
    }
    
    Serial.print(F("Warning: I2C failed attempt in GetByte("));
    Serial.print(Addr, HEX);
    Serial.println(F(")"));
  }

  Serial.print(F("    ")); Serial.print(i); Serial.println(F(" failed I2C attempts"));
  Serial.println(F("Error: I2C comm failure in GetByte()"));
  *ptr = 0;
  return false;
}

/*
   @param Register- the register where you want to read memory from
*/
byte Pmod::ReadByte(byte Register) {
  byte Memory;
  if (GetByte(Register, &Memory)) {             //takes in the register you want to read memory from and address of the variable: Memory
    //Serial.print(F("Getting @")); Serial.println(Register);
    //Serial.print(F("\tValue: ")); Serial.println(Memory);
    return Memory;                              //returns the byte
  }
  else {
    Serial.println(F("Error in ReadByte: No data returned over I2C"));
    return 0xFF;
  }
}

/*
  @param Starting_addr - the starting address for the register to be read as a word
   This function 1st reads the memory at the starting addr and then the memmory
   at the next address. Since its returing a long (32 bits) the byte (8 bits) data types are turned into a
   long and the upper byte is shifted to the left by 8 bits and then ordered with the
   lower byte to have one single return value of (32 bits). It is then AND'd with 0xFFFF
   to remove the first 16 bits (all zeros) and sends back the last 16 bits (the memory)
*/
long Pmod::ReadRegister(byte Starting_addr) {
  byte Upper_bytes = ReadByte(Starting_addr);       //Reads the upper bits from the starting register and store it in varaible
  byte Lower_bytes = ReadByte(Starting_addr + 1);   //Reads the next register and stores it in variable
  return (Upper_bytes << 8 | Lower_bytes) & 0xFFFF; //shifts the Upper byte to the left to allow the lower byte to be OR'd
}

/*
  returns Status Register data
*/
byte Pmod::ReadStatusRegister() {
  return ReadByte(Status_reg);
}
/*
  Returns Control Register data
*/
long Pmod::ReadControlRegister() {
  return ReadRegister(Ctrl_reg);
}
/*
  This function gets the memory at control register 2
  and sets the Reset bit (D4) with 0x10 -> Bitwise operations
*/
void Pmod::Reset() {
  byte Reset;
  byte Ctrl_reg2 = Ctrl_reg + 1;
  bool configSuccess;
  
  if (GetByte(Ctrl_reg2, &Reset)) {
    Reset |= 0x10;
    configSuccess = SetByte(Ctrl_reg2, Reset);
    if (!configSuccess) {
      Serial.println(F("Pmod Reset failed"));
    }
  }
  else {
    Serial.println(F("Error in Reset: No data returned over I2C"));
  }
}
/*
  @param mode - takes in the defined const for modes located in the header file
  This function reads the memory of the first control register. The first 4 bits
  of this register (d15-d12) control the mode of the imepdance analyzer. To change
  the mode bits without changing the last 4 bits (d11-d7). I AND/equaled the temp
  variable. temp = 0xNN & 0x0F = 0x0N, where N is some value. This new value is then
  or'd with the mode defined value to change the bits.
*/
void Pmod::SetMode(byte IAMode) {
  byte temp = ReadByte(Ctrl_reg);
  temp &= 0x0F;
  temp |= IAMode;
  if (SetByte(Ctrl_reg, temp) == true) {
    return ;
  }
  else {
    Serial.print(F("Mode Set Unsuccessful: ")); Serial.print(temp, HEX);
    Serial.print(F(" to address ")); Serial.println(Ctrl_reg, HEX);
  }
}

/*
  @param excit_voltage - takes a constant byte code to configure the excitation voltage
                              (output to impedance being measured)
      Code values   0 : 2.0 Vpp
                    1 : 0.2 Vpp
                    2 : 0.4 Vpp
                    3 : 1.0 Vpp
  @param calibration_freq - takes a long integer representing the frequency (in Hz) to start
                            the frequency sweep from
  @param freq_increment - takes a long integer representing the frequency increment (in Hz)
                          to configure for frequency sweeps
  @param num_increments - takes an integer representing the number of frequency increments
                          to take for each frequency sweep

  This function configures the ADC excitation voltage, sweep start frequency, sweep
  interval, and number of intervals. It should be performed once before doing any sweeps.
*/
void Pmod::SetSweepParameters(byte excit_voltage, long calibration_freq,
                              long freq_increment, int num_increments) {
  bool configSuccess;
  
  SetMode(Standby);           //setting to standby mode
  if (PMOD_DEBUG_OUTPUT) {
    Serial.print(F("SetSweepParameters called ("));
    Serial.print(excit_voltage); Serial.print(F(","));
    Serial.print(calibration_freq); Serial.print(F(","));
    Serial.print(freq_increment); Serial.print(F(","));
    Serial.print(num_increments); Serial.println(F(")"));
  }
  
  byte temp = ReadByte(Ctrl_reg);
  temp |= (PGA_GAIN & 0x01);  //setting PGA gain
  temp &= 0xF9;
  temp |= excit_voltage;      //setting excitation voltage code
  configSuccess = SetByte(Ctrl_reg, temp);
  if (!configSuccess)
    Serial.println(F("Error in SetSweepParameters: Failed to set excitation voltage"));
  Excit_voltage = excit_voltage;
  
  SetStartFreq(calibration_freq);   //configuring sweep start frequency
  SetFreqIncrement(freq_increment); //configuring sweep increment frequency
  SetNumIncrements(num_increments); //configuring number of sweep increments
}

/*
  @param excit_voltage - output location for constant byte code of the excitation voltage
  @param calibration_freq - output location for long integer representing the starting 
                            frequency (in Hz) of a frequency sweep 
  @param freq_increment - output location for long integer representing the frequency 
                          increment (in Hz) of a frequency sweep
  @param num_increments - output location for integer representing the number of increments
                          of a frequency sweep
*/
void Pmod::GetSweepParameters(byte& excit_voltage, long& calibration_freq, 
                              long& freq_increment, int& num_increments)
{
  excit_voltage = Excit_voltage;
  calibration_freq = Sweep_calibration_freq;
  freq_increment = Sweep_freq_increment;
  num_increments = Sweep_num_increments;
}

/*
  @param start_freq - takes a long integer representing the starting frequency
                            (in Hz) of a frequency sweep (24 bits)

  This function configures the appropriate Pmod registers with the code to use a starting
  frequency of `start_freq` for a frequency sweep.
  *** Changes made to the start frequency must also be reflected in changes to the gain 
      factor class variables, WHICH REQUIRES A RECALIBRATION
*/
void Pmod::SetStartFreq(long start_freq) {
  long StartFreq = (double(start_freq) / (Internal_clock / 4.0)) * pow(2, 27);
  byte high = (StartFreq >> 16) & 0xFF;
  byte mid = (StartFreq >> 8) & 0xFF;
  byte low = StartFreq & 0xFF;
  bool configSuccess;

  if (PMOD_DEBUG_OUTPUT) {
    Serial.print(F("SetStartFreq called (")); 
    Serial.print(start_freq); 
    Serial.print(F("), "));
  }
  
  //Serial.print(F("start freq = ")); Serial.print(high);
  //Serial.print(F("; ")); Serial.print(mid);
  //Serial.print(F("; ")); Serial.print(low); Serial.println(F(" "));
  configSuccess = true;
  configSuccess &= SetByte(0x82, high);
  configSuccess &= SetByte(0x83, mid);
  configSuccess &= SetByte(0x84, low);

  if (PMOD_DEBUG_OUTPUT) {
    Serial.print(F("success = ")); 
    Serial.println(configSuccess);
  }

  Sweep_calibration_freq = start_freq;
}

/*
  @param freq_increment - takes a long integer representing the frequency increment
                          (in Hz) to be added to the frequency sweep each interval (24 bits)

  This function configures the appropriate Pmod registers with the code to use a frequency
  increment of `freq_increment` for a frequency sweep
  *** Changes made to the frequency increment must also be reflected in changes to the
      Gain_factor_highf class variable, WHICH REQUIRES A RECALIBRATION
*/
void Pmod::SetFreqIncrement(long freq_increment) {
  long FreqIncr = (double(freq_increment) / (Internal_clock / 4.0)) * pow(2, 27);
  byte high = (FreqIncr >> 16) & 0xFF;
  byte mid = (FreqIncr >> 8) & 0xFF;
  byte low = FreqIncr & 0xFF;
  bool configSuccess;

  if (PMOD_DEBUG_OUTPUT) {
    Serial.print(F("SetFreqIncrement called (")); 
    Serial.print(freq_increment); 
    Serial.print(F("), "));
  }

  configSuccess = true;
  configSuccess &= SetByte(0x85, high);
  configSuccess &= SetByte(0x86, mid);
  configSuccess &= SetByte(0x87, low);
  if (PMOD_DEBUG_OUTPUT) {
    Serial.print(F("success = ")); 
    Serial.println(configSuccess);
  }

  Sweep_freq_increment = freq_increment;
}

/*
  @param num_increments - takes an integer representing the number of frequency increments
                          (one less than the number of points) to be used in the frequency
                          sweep (9 bits)

  This function configures the appropriate Pmod registers to use `num_increments` points
  when doing each frequency sweep
  *** Changes made to the number of increments must also be reflected in changes to the
      Gain_factor_highf class variable, WHICH REQUIRES A RECALIBRATION
*/
void Pmod::SetNumIncrements(int num_increments) {
  byte high = (num_increments >> 8) & 0xFF;
  byte low = num_increments & 0xFF;
  bool configSuccess;

  if (PMOD_DEBUG_OUTPUT) {
    Serial.print(F("SetNumIncrements called (")); 
    Serial.print(num_increments); 
    Serial.print(F("), "));
  }

  configSuccess = true;
  configSuccess &= SetByte(0x88, high);
  configSuccess &= SetByte(0x89, low);
  if (PMOD_DEBUG_OUTPUT) {
    Serial.print(F("success = ")); 
    Serial.println(configSuccess);
  }

  Sweep_num_increments = num_increments;
}

/*
  @param real_data - measured real component of impedance from the ADC
  @param imag_data - measured imaginary component of impedance from the ADC
  @param known_impedance - actual (theoretical) impedance value in ohms of the component
                            measured for calibration
  @param set_low - flag to indicate whether the passed data represents the low or high end
                    of a frequency sweep (setting the low or high gain factor); default 
                    value is true

  This function performs one-time setting of the gain factor when the AD5933 is measuring
    a known impedance.
  See pg. 17 of the AD5933 data sheet (Rev. F) for reference
*/
/*void Pmod::SetGainFactor(double real_data, double imag_data, double known_impedance,
                         bool set_low) {
  double Impedance_magnitude = sqrt(real_data * real_data + imag_data * imag_data);
  if (set_low)
    Gain_factor_lowf = 1 / (Impedance_magnitude * known_impedance);
  else
    Gain_factor_highf = 1 / (Impedance_magnitude * known_impedance);
}*/

/*
  @param real_data - pointer to array of measured real components of impedance from the ADC;
                      must be at least `Sweep_num_increments+1` elements long
  @param imag_data - pointer to array of measured imaginary components of impedance from the ADC;
                      must be at least `Sweep_num_increments+1` elements long
  @param known_impedance - actual (theoretical) impedance value in ohms of the component
                            measured for calibration

  This function performs one-time setting of the gain factor array when the AD5933 is measuring
    a known impedance; a gain factor is calculated for each frequency since linear interpolation 
    (two point calibration) was inaccurate over large frequency ranges
  See pg. 17 of the AD5933 data sheet (Rev. F) for reference
*/
void Pmod::SetGainFactors(float *real_data, float *imag_data, double known_impedance) {
  double Impedance_magnitude;
  int i;

  if (Gain_factor_buf == NULL) {
    Serial.println(F("Error setting gain factors: gain buffer uninitalized for this Pmod instance"));
    return;
  }

  Serial.println(F("Setting gain factors..."));
  for (i=0; i <= Sweep_num_increments; i++) {
    //Serial.print(F("i=")); Serial.print(i); Serial.print(F(", "));
    Impedance_magnitude = sqrt(real_data[i] * real_data[i] + imag_data[i] * imag_data[i]);
    Gain_factor_buf[i] = 1 / (Impedance_magnitude * known_impedance);
    //printScientific(Gain_factor_buf[i]); Serial.println("");
  }
}

/*
  @param real_data - measured real component of impedance from the ADC
  @param imag_data - measured imaginary component of impedance from the ADC
  @param set_low - flag to indicate whether the passed data represents the low or high end
                    of a frequency sweep (setting the low or high gain factor); default 
                    value is true

  This function performs one-time setting of the internal AD5933 phase when the AD5933 is 
    measuring a known resistance (the function assumes the nominal measurement phase angle is 0)
  See pg. 19-20 of the AD5933 data sheet (Rev. F) for reference
*/
void Pmod::SetSystemPhase(double real_data, double imag_data, bool set_low) {
  double System_phase = atan2(imag_data, real_data);
  if (set_low)
    System_phase_lowf = System_phase;
  else
    System_phase_highf = System_phase;
}

/*
  @param Real_data - pointer to an array of float values, needed for internal data storage;
                      The function will output the measured real component of impedance from 
                      the ADC to this buffer; Real_data must be at least `Sweep_num_increments+1` elements long
  @param Imag_data - pointer to an array of float values, needed for internal data storage;
                      The function will output the measured imaginary component of impedance 
                      from the ADC to this buffer; Real_data must be at least `Sweep_num_increments+1` 
                      elements long
  @param Gain_factors - pointer to an array of double values, needed for instance data storage;
                          this function call will not modify the values pointed by Gain_factors,
                          but will assign the pointer to the Gain_factor_buf instance variable
                          Must be at least Sweep_num_increments+1 elements long
  @param excit_voltage - takes a constant byte code to configure the excitation voltage
  @param calibration_freq - takes a long integer representing the frequency (in Hz) to start
                            the frequency sweep from
  @param freq_increment - takes a long integer representing the frequency increment (in Hz)
                          to configure for frequency sweeps
  @param num_increments - takes an integer representing the number of frequency increments
                          (one less than the number of points) to be used in the frequency
                          sweep (9 bits)
  @param known_impedance - takes a double prec float with the actual value of the
                            calibration impedance

  This function calls existing functions to initialize the frequency sweep parameters
    and calculates the gain scale factor using multi-point calibration
  The calibration resistor should be in place on the Pmod when this function is called
*/
void Pmod::Calibrate(float *Real_data, float *Imag_data, double *Gain_factors,
                      byte excit_voltage, long calibration_freq, long freq_increment,
                      int num_increments, double known_impedance) {
  double temp;
  int i;
  
  if (PMOD_DEBUG_OUTPUT) {
    Serial.println(F("Calibrate called (...)"));
  }

  if (Real_data == NULL || Imag_data == NULL || Gain_factors == NULL) {
    Serial.println(F("Error in calibration: output buffer pointer(s) have no target"));
    exit(1);
  }
  if (num_increments == 0) {
    Serial.println(F("Error in calibration: num_increments is 0"));
    exit(1);
  }
  if (known_impedance == 0) {
    Serial.println(F("Error in calibration: bad known impedance value"));
    exit(1);
  }

  if (PMOD_DEBUG_OUTPUT) {
    Serial.println(F("Calibrate() arg checks complete."));
  }

  Gain_factor_buf = Gain_factors;
  SetSweepParameters(excit_voltage, calibration_freq, freq_increment, num_increments);
  DoFreqSweep(Real_data, Imag_data);
  
  //SetGainFactor(Real_data[0], Imag_data[0], known_impedance, true);  
  //SetGainFactor(Real_data[num_increments], Imag_data[num_increments], known_impedance, false);
  SetGainFactors(Real_data, Imag_data, known_impedance);
  SetSystemPhase(Real_data[0], Imag_data[0], true);
  SetSystemPhase(Real_data[num_increments], Imag_data[num_increments], false);

  // Output calcuated gain & phase factors
  Serial.print(F("Low sweep freq = ")); Serial.println(Sweep_calibration_freq); 
  Serial.print(F("High sweep freq = ")); Serial.println(Sweep_calibration_freq + Sweep_freq_increment*Sweep_num_increments);
  Serial.print(F("Gain factor (low freq) = "));
  printScientific(Gain_factor_buf[0]); Serial.println("");

  Serial.print(F("Gain factor (high freq) = "));
  printScientific(Gain_factor_buf[Sweep_num_increments]); Serial.println("");

  Serial.print(F("System phase (low freq) = ")); Serial.print(System_phase_lowf, 6); Serial.println(F(" rad"));
  Serial.print(F("System phase (high freq) = ")); Serial.print(System_phase_highf, 6); Serial.println(F(" rad"));
}

/*
  @param real_data - pointer to array of floats to output the real parts of measured
                        impedances; real_data must be at least `Sweep_num_increments+1` elements
                        long (previously set by SetNumIncrements() )
  @param imag_data - pointer to array of floats to output the imaginary parts of measured
                        impedances; imag_data must be at least `Sweep_num_increments+1` elements
                        long (previously set by SetNumIncrements() )

  This function performs a frequency sweep with the Pmod, following the procedure in
    Figure 28 (page 22) of the AD5933 data sheet (Rev. F)
  The AD5933 should be powered down after calling this function if it is going to be idle
*/
void Pmod::DoFreqSweep(float *real_data, float *imag_data) {
  byte temp;
  long regData;
  long RealData, ImagData;
  double DFT_Magnitude, GainFactor;
  int loopIter = 0;
  bool configSuccess;

  // Paranoia - read sweep parameters from Pmod configuration registers, 
  //  confirm they are correct
  temp = ReadByte(0x84); regData = temp;
  temp = ReadByte(0x83); regData |= temp<<8;
  temp = ReadByte(0x82); regData |= temp<<16;
  Serial.print(F("Start frequency code: ")); Serial.println(regData, HEX);
  temp = ReadByte(0x87); regData = temp;
  temp = ReadByte(0x86); regData |= temp<<8;
  temp = ReadByte(0x85); regData |= temp<<16;
  Serial.print(F("Frequency increment code: ")); Serial.println(regData, HEX);
  temp = ReadByte(0x89); regData = temp;
  temp = ReadByte(0x88); regData |= temp<<8;
  Serial.print(F("Num Increments: ")); Serial.println(regData, HEX);
  

  // Put the AD5933 in standby mode
  SetMode(Standby);
  delay(50);

  // Enter initialize with start frequency command in control register
  //Serial.println(ReadRegister(0x88));
  SetMode(Init_with_start_freq);
  delay(50);

  // Set settling time to allow impedance to settle (511*4 cycles max amount of time)
  //    See Table 13 for reference
  switch (MEAS_DELAY_MULTIPLIER) {
    case 1:
      temp = 0x00; break;
    case 2:
      temp = 0x02; break;
    case 4:
      temp = 0x06; break;
    default:
      Serial.println(F("Invalid delay multiplier in DoFreqSweep()"));
      return;
  }
  
  configSuccess = true;
  temp |= MEAS_DELAY_CYCLES>>8 & 0x1;
  configSuccess &= SetByte(0x8A, temp);
  temp = MEAS_DELAY_CYCLES & 0xFF;
  configSuccess &= SetByte(0x8B, temp);
  if (!configSuccess) {
    Serial.println("Error in DoFreqSweep: could not configure settling time");
  }

  // Enter start frequency sweep mode
  SetMode(Start_freq_sweep);
  if (PMOD_DEBUG_OUTPUT) {
    Serial.print(F("Status reg after sweep start: ")); 
    Serial.println(ReadStatusRegister(), HEX);
  }

  Serial.println(F("xxxxxxxxxxxxxxxxxxxxxxxx"));
  // Perform frequency sweep loop  
  while (1) {
    // Poll status register for valid real/imaginary data to check for DFT completion
    while ( !(ReadStatusRegister() & D1) )
      ;

    // Read measured real and imaginary values
    RealData = ReadRegister(Real_data_reg);
    ImagData = ReadRegister(Imag_data_reg);

    if (RealData & 0x8000)    // signed - value is negative
      RealData = RealData - 0x10000;
    if (ImagData & 0x8000)    // signed - value is negative
      ImagData = ImagData - 0x10000;
    // Raw impedance data
    if (PMOD_RAW_DATA_OUTPUT) {
      Serial.print(RealData); Serial.print(F(" ")); Serial.println(ImagData);
    }

    // Store measured impedance data for analysis upstream
    real_data[loopIter] = RealData;
    imag_data[loopIter] = ImagData;

    // Check if frequency sweep is complete; if so, stop looping
    delay(100);                            // wait on PMOD to read end-of-sweep flag
    temp = ReadStatusRegister();
    if (temp & D2) {
      if (PMOD_DEBUG_OUTPUT) {
        Serial.print(F("SR = ("));
        Serial.print(temp, HEX);
        Serial.print(F(" / "));
      }
      
      delay(500);

      if (PMOD_DEBUG_OUTPUT) {
        Serial.print(ReadStatusRegister(), HEX);
        Serial.println(F(")"));
      }
      break;
    }

    // If Pmod wants to do more increments than we have programmed it to (for some reason?
    //    Maybe bad configuration or I2C failure?) then ignore it, rather than reading until
    //    it overflows our buffer
    if (loopIter >= Sweep_num_increments) {
      Serial.println(F("Warning: Pmod configured for longer sweep than expected in DoFreqSweep(); remaining increments skipped"));
      break;
    }

    // Program increment frequency command to control register
    SetMode(Inc_freq);
    loopIter++;
  }
  Serial.println(F("xxxxxxxxxxxxxxxxxxxxxxxx"));

  // Go into power-down mode
  //SetMode(Power_down);
}

/*
  @param real_data - pointer to array of floats to output the real parts of measured
                        impedances; real_data must be at least `num_vals` elements long
  @param imag_data - pointer to array of floats to output the imaginary parts of measured
                        impedances; imag_data must be at least `num_vals` elements long
  @param frequencies - pointer to array of long integers containing the frequencies (in Hz)
                        corresponding to measured impedances; must be at least `num_vals`
                        elements long
  @param impedance_mag - pointer to array of doubles to output the actual impedance magnitudes
                        corresponding to the values input in real_data and imag_data;
                        impedance_mag[] must be at least num_vals elements long
  @param impedance_phase - pointer to array of doubles to output the actual impedance phases 
                        corresponding to the values input in real_data and imag_data;
                        impedance_phase[] must be at least num_vals elements long
  @param num_vals - integer number of elements to calculate impedances for

  This function calculates measured impedances (in Ohms) from the values measured by the
    AD5933. See pg. 17 of AD5933 data sheet for reference.
*/
void Pmod::GetImpedances(float *real_data, float *imag_data, long *frequencies, 
                          double *impedance_mag, double *impedance_phase, int num_vals) {
  int i;
  double Impedance_magnitude, Impedance_phase, Gain_factor, System_phase;
  long Min_freq, Max_freq;

  if (Gain_factor_buf == NULL) {
    Serial.println(F("Error calculating impedance: Gain_factor_buf has no target"));
    return;
  }
  if (System_phase_lowf == 0 && System_phase_highf == 0) {
    Serial.println(F("Error calculating impedance: system phase is uninitialized"));
    return;
  }
  if (Sweep_calibration_freq == 0) {
    Serial.println(F("Error calculating impedance: `Sweep_calibration_freq` is uninitialized"));
    return;
  }
  if (Sweep_freq_increment == 0) {
    Serial.println(F("Error calculating impedance: `Sweep_calibration_freq` is uninitialized"));
    return;
  }
  if (Sweep_num_increments == 0) {
    Serial.println(F("Error calculating impedance: `Sweep_num_increments` is uninitialized"));
    return;
  }
  
  Min_freq = Sweep_calibration_freq;
  Max_freq = Sweep_calibration_freq + Sweep_freq_increment*Sweep_num_increments;
  for (i = 0; i < num_vals; i++) {
    Impedance_magnitude = sqrt(real_data[i] * real_data[i] + imag_data[i] * imag_data[i]);
    Impedance_phase = atan2(imag_data[i], real_data[i]);
    
    // Get the gain factor to be used by mapping the gain linearly based on the mapping of 
    //  the measurement's frequency in relation to the max and min freq for a sweep
    if (Gain_factor_buf[i] == 0) {
      Serial.print(F("Error calculating impedance: `Gain_factor_buf` is uninitialized for i=")); Serial.print(i); Serial.println(F(""));
      return;
    }
    //Gain_factor = mapDouble((double)frequencies[i], (double)Min_freq, (double)Max_freq, Gain_factor_lowf, Gain_factor_highf);
    Gain_factor = Gain_factor_buf[i];
    System_phase = mapDouble((double)frequencies[i], (double)Min_freq, (double)Max_freq, 
                            System_phase_lowf, System_phase_highf);
    
    impedance_mag[i] = 1 / (Impedance_magnitude * Gain_factor);
    impedance_phase[i] = Impedance_phase - System_phase;

    /* Unit testing
    Serial.print(F("System_phase = ")); Serial.println(System_phase, 6);
    Serial.print(real_data[i]); Serial.print(' '); Serial.println(imag_data[i]);
    Serial.print(impedance_mag[i]); Serial.print(' '); Serial.println(impedance_phase[i], 6);
    Serial.println();*/
  }
}

/*
  @param val - value to be mapped
  @param fromL - lower bound of value's current range
  @param fromH - upper bound of value's current range
  @param toL - lower bound of value's target range
  @param toH - upper bound of value's target range

  This function maps a value `val` from the `from` range to the `to` range; behaves the same 
    as the map() Arduino library function, using doubles instead of long ints
 */
double Pmod::mapDouble(double val, double fromL, double fromH, double toL, double toH) {
  return (val - fromL) * (toH - toL) / (fromH - fromL) + toL;
}
