#include "Arduino.h"
#include "SDCARD.h"
#include <SPI.h>
#include <SD.h>

Sdcard::Sdcard() {
  SD.begin(4);        //what pin the CS pin is connected to 

}

void Sdcard::WriteFile(long outData) {
  File  dataFile = SD.open("ee494.txt", FILE_WRITE);      //ee494.txt is the name of the file to write to 
  if (dataFile) {
    dataFile.print(F("Impedance: "));
    dataFile.println(outData);
    dataFile.close();
  }

  else {
    dataFile.close();
    Serial.println(F("Error writing"));
  }
}

void Sdcard::ReadFile() {
  File dataFile = SD.open("ee494.txt");

  if (dataFile) {
    while (dataFile.available()) {
      Serial.write(dataFile.read());
    }
    dataFile.close();
  }
  else {
    Serial.println(F("error opening file"));
  }
}
